# Portfolio Designs

![main image](main_image.jpg)

I furnished a couple of designs for a portfolio website with many variations for each design. Frustrated with presenting a large collection of images, I took a handful of designs and converted them into a website. The main area of interest is the ability to switch between designs and then select variations on that particular style. 

![different designs with variations](second_image.jpg)

The mechanism to change styles primarily involves switching `*.css` files. Multiple full *css* files are a struggle to maintain so I edited the original site to work with applied classes. You can see a working version [here](https://lawrencechin.gitlab.io/portfolio_designs/). Hit the *options* button in the footer to change styles.

Additionally, if you wish to see some more designs point your eyes [here](https://lawrencechin.gitlab.io/portfolio_designs/designs).
