var Navigation = (function() {
	
	var mainNav = $('nav.main'),
	linkage = mainNav.find('ul'),
	loadingDiv = $('div.loading_div'),
	holdingDiv = $('div.holding_div'),
	sideShadow = $('div.side_shadow');


	function init() {
		mainMenu();
		linkage.on('click', 'a', function(e){
			var $this = $(this);
			e.preventDefault();
			mainNav.parent('div.sidebar').addClass('newPosition');
			$this.parent('li').siblings('li').removeClass('active').end().addClass('active');
			loadingDiv.addClass('test');
			holdingDiv.load($this.attr('href') + ' section.' + $this.data('role'), function(){
				loadingDiv.removeClass('test');
				workMenu();
				holdingDiv.prepend(sideShadow);
				holdingDiv.find('section.main').find('nav.project_navigation').addClass('newPosition');
			});
		});

		mainNav.parent('div.sidebar').find('h1.logo').on('click', 'a', function(e){
			var $this = $(this);
			e.preventDefault();
			mainNav.parent('div.sidebar').addClass('newPosition');
			$this.parent('li').siblings('li').removeClass('active').end().addClass('active');
			loadingDiv.addClass('test');
			holdingDiv.load($this.attr('href') + ' section.' + $this.data('role'), function(){
				loadingDiv.removeClass('test');
				workMenu();
				holdingDiv.prepend(sideShadow);
				holdingDiv.find('section.main').find('nav.project_navigation').addClass('newPosition');
			});
		});

		$('ul.project_nav_second').on('click', 'a', function(e){
			var $this = $(this);
			e.preventDefault();
			$this.parent('li').siblings('li').removeClass('active').end().addClass('active');
			loadingDiv.addClass('test');
			holdingDiv.load($this.attr('href') + ' section.' + $this.data('role'), function(){
				loadingDiv.removeClass('test');
				AppShowcase.init();

			});
		});

		//temp option for testing - removewhen eveything works//
		$.ajaxSetup({
   			cache: false
 		});
	}

	function workMenu(){
		$('nav.project_navigation ul li').on({
			click: function(e){
				var $this = $(this);
				$this.parent('li').siblings('li').removeClass('fadeProjectNav');
				e.preventDefault();
				loadingDiv.addClass('test');
				holdingDiv.load($this.attr('href') + ' section.project' , function(){
					loadingDiv.removeClass('test');
      				AppShowcase.init();
      				holdingDiv.prepend(sideShadow);
				});
			},

			mouseenter: function(){
				$(this).parent('li').siblings('li').addClass('fadeProjectNav');
			},

			mouseleave: function(){
				$(this).parent('li').siblings('li').removeClass('fadeProjectNav');
			}

		}, 'a');
	}

	function mainMenu(){
		holdingDiv.find('nav.project_navigation ul li').on({
			click: function(e){
				var $this = $(this);
				$this.parent('li').siblings('li').removeClass('fadeProjectNav');
				e.preventDefault();
				mainNav.parent('div.sidebar').addClass('newPosition');
				loadingDiv.addClass('test');
				holdingDiv.load($this.attr('href') + ' section.project' , function(){
					loadingDiv.removeClass('test');
      				AppShowcase.init();
      				holdingDiv.prepend(sideShadow);
				});
			},

			mouseenter: function(){
				$(this).parent('li').siblings('li').addClass('fadeProjectNav');
			},

			mouseleave: function(){
				$(this).parent('li').siblings('li').removeClass('fadeProjectNav');
			}

		}, 'a');
	}

	return { init : init };

})();

var AppShowcase = (function() {
	
	var $el = '',
		// device element
		$device = '',
		// the device image wrapper
		$trigger = '',
		// the screens
		$screens = '',
		// the device screen image
		$screenImg = '',
		// HTML Body element
		$body = $( 'body' ),
		$backWork = ''; 

	function init() {
		populateVariables();
		// show grid
		$trigger.on( 'click', showGrid );
		// when a grid´s screen is clicked, show the respective image on the device
		$screens.on( 'click', function(e) {
			showScreen( $( this ) );
			e.preventDefault();
		} );
		gridNumbers();
		backToWork();
	}

	function populateVariables(){
		//I guess the self involking variable looks for the variables on first run and doesn't update - need a better method for this in the future

		$el = $('div.project_main', 'div.holding_div');
		$device = $el.find('div.main_image');
		$trigger = $device.children( 'a:first' );
		$screens = $el.find( 'div.thumb_grid > a' );
		$screenImg = $device.find( 'img' );
		$backWork = $el.find('div.image_description');
	}

	function showGrid() {
		$el.addClass( 'grid_view' );
		// clicking somewhere else on the page closes the grid view
		$body.off( 'click' ).on( 'click', function() { showScreen(); } );
		return false;
	}

	function showScreen( $screen ) {
		if( $screen ) {
			// update image and title on the device
			$screenImg.attr( 'src', $screen.attr( 'href' ) );
		} else {
			$el.removeClass( 'grid_view' );
		}
	}

	function gridNumbers(){
		var gridLength = $screens.length;
		while(gridLength % 3 !== 0){
			gridLength++;
			$('<a href="#"><img src="./images/demo1/thumb.png" /></a>').appendTo($screens.parent('div'));
		}
	}

	function backToWork(){
		$backWork.on('click', 'a', function(e){
			e.preventDefault();
			$("nav.main a[data-role='work']").trigger('click');
		});

		$el.find('div.image_description_two').on('click', 'a', function(e){
			e.preventDefault();
			$("nav.main a[data-role='work']").trigger('click');
		});
	}

	return { init : init };

}) ();

var Options = (function(){

	var footer = $('footer'),
	sectionOptions = $('section#options'),
	boxOptions = sectionOptions.find('div.box_options'),
	navOptionLinks = boxOptions.find('nav.layouts'),
	haiche4 = navOptionLinks.find('h4'),
	bodyish = $('body'),
	headerFish = $('div.sidebar header'),
	imageLion = $('div.sidebar h1.logo img'),
	holdMyDiv = $('div.holding_div');

	function init(){
		footer.on('click', 'button', function(){

			sectionOptions.addClass('show');

			boxOptions.addClass('margin').find('button.clo').on('click', function(){
					$(this).parent('div').removeClass('margin').parent('section').removeClass('show').end().end().off();	
					});

			boxOptions.find('button.min').off().on('click', function(){

				if (boxOptions.hasClass('minimise')){
					sectionOptions.removeClass('min');
					boxOptions.removeClass('minimise');
					$(this).text('Minimise');

				} else {

					boxOptions.addClass('minimise');
					sectionOptions.addClass('min');
					$(this).text('Maximise');
				}
			});

			boxOptions.find('button.back').off().on('click', function(){
				$('li.design').removeClass('hide').removeClass('expand');
				haiche4.removeClass('show');

				boxOptions.removeClass('shrink');

				$('ul', 'li.design').each(function() {
  					$(this).removeClass('show');
				});

			})
		});

		styleSwitcher();
	}

	function styleSwitcher(){

		var mekon = navOptionLinks.find('a').not('ul ul ul a');

		mekon.on('click', function(e){
			var $this = $(this);
			e.preventDefault();

			boxOptions.addClass('shrink');
			bodyish.attr('class', '');
			headerFish.attr('class', '');
			imageLion.attr('class', '');
			holdMyDiv.attr('class', 'holding_div');

			if($this.parent('li').hasClass('design last')){

				$('ul#backgrounds').off().on('click', 'a', function(e){
					var $this = $(this);
					e.preventDefault();
					bodyish.attr('class', '').addClass('bck_'+$this.attr('href'));
				});

				$('ul#logos').off().on('click', 'a', function(e){
					var $this = $(this);
					e.preventDefault();
					// console.log($('body[class*="bck_"]'));
					// console.log(bodyish.attr('class').indexOf('bck_'));
					headerFish.attr('class', '').addClass('logo_'+$this.attr('href'));
					imageLion.attr('class', '').addClass('logo_'+$this.attr('href'));
					holdMyDiv.attr('class', 'holding_div').addClass('logo_'+$this.attr('href'));	
				});
			}

			$('link.king').attr('href', $this.attr('href'));

			$('span', navOptionLinks).each(function() {
  				$(this).removeClass('active');
			});

			if($this.parent('li').hasClass('design')){
				$this.parent('li').siblings('li').addClass('hide').end().addClass('expand').find('ul').addClass('show');
				haiche4.addClass('show');
				$this.find('span').addClass('active');

			} else {

				$this.find('span').addClass('active').parents('li.design').children().children('span').addClass('active');
			}
		});
	}

	return { init : init };

})();